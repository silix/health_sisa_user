# -*- coding: utf-8 -*-
#This file is part health_sisa_user module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from .health_sisa_user import *


def register():
    Pool.register(
        User,
        module='health_sisa_user', type_='model')
